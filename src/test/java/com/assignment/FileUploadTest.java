package com.assignment;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests FileUpload functionality against available input and otput.
 */
@RunWith(JUnitParamsRunner.class)
public class FileUploadTest
{

  @Test
  @Parameters({
      "exampleA_input.csv, exampleA_output.csv",
      "exampleB_input.csv, exampleB_output.csv",
      "exampleC_input.csv, exampleC_output.csv"
  })
  public void testProcessInput(String input, String output) throws FileNotFoundException
  {
    URL inputUrl = Thread.currentThread().getContextClassLoader().getResource(input);
    URL outputUrl = Thread.currentThread().getContextClassLoader().getResource(output);
    File inputFile = new File(inputUrl.getPath());
    File outputFile = new File(outputUrl.getPath());

    BufferedReader in = new BufferedReader(new FileReader(inputFile));
    BufferedReader out = new BufferedReader(new FileReader(outputFile));

    Stream<String> inputCsv = in.lines();
    Stream<String> expectedCsv = out.lines();

    List<String[]> inputCsvData = FileUpload.parseCsvDataAsString(inputCsv);
    List<int[]> inputResults = FileUpload.processInput(inputCsvData);

    // read data as it is in the file without any modification
    List<String[]> expectedCsvData = FileUpload.parseCsvDataAsString(expectedCsv);
    List<int[]> expectedData = FileUpload.convertCsvStringsToInt(expectedCsvData.stream());

    assertEquals("Result is different from expected in number of rows", inputResults.size(), expectedData.size());

    for (int i = 0; i < inputResults.size(); i++)
    {
      assertTrue("Result is different from expected in content of rows", Arrays.equals(inputResults.get(i), expectedData.get(i)));

    }
  }

  @Test
  public void testCleanData()
  {

    // TODO: implement test
  }

  @Test
  public void testIdentifyFminFmax()
  {
    // TODO: implement test
  }
}
