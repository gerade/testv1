package com.assignment;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handles fileupload form POST request, parses input and writes output directly to response writer.
 *
 * @author Igor Polietaiev
 */
public class FileUpload extends HttpServlet
{

  private static final Logger LOGGER = Logger.getLogger(FileUpload.class.getName());
  private static final String CSV_COMMA_SEPARATOR = ",";
  /**
   * id column is always in the beginning.
   * TODO: identify it based on csv header names.
   */
  private static final byte PRE_DATA_ROWS = 1;
  /**
   * decision column at the end.
   * TODO: identify it based on csv header names.
   */
  private static final byte POST_DATA_ROWS = 1;
  private static final int DECISION_0 = 0;
  private static final String TAB_SEPARATOR = "\t";
  private static final String LINE_SEPARATOR = System.getProperty("line.separator");
  public static final String TAB_DELIMITER = "\t";

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {

    // TODO: check XSRF token from client here.

    ServletFileUpload upload = new ServletFileUpload();
    upload.setSizeMax(5000 * 1000);// 5000 kb in bytes
    response.setContentType("text/plain");

    try
    {
      FileItemIterator iter = upload.getItemIterator(request);

      while (iter.hasNext())
      {
        FileItemStream item = iter.next();

        if (item.isFormField())
        {
          // skip form fiels, which are not uploaded files
          continue;
        }

        InputStream inputStream = item.openStream();

        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

        Stream<String> inputCsv = in.lines();

        // input CSV
        List<String[]> inputCsvData = parseCsvDataAsString(inputCsv);
        if (inputCsvData.isEmpty())
        {
          // TODO provide feedback to the user, regarding problematic input file
          LOGGER.log(Level.INFO, "Malformed CSV input.");
        }
        String[] header = inputCsvData.get(0);

        List<int[]> result = processInput(inputCsvData);

        response.getWriter().write(formatOutput(header, result));

        // TODO: forward result back to index.jsp

      }
    }
    catch (FileUploadException e)
    {
      // TODO provide feedback to the user, regarding problematic input file
      LOGGER.log(Level.INFO, "Uploaded image is too big", e);
    }
    catch (NumberFormatException e)
    {
      LOGGER.log(Level.INFO, "Wrong uploaded image format", e);
    }
  }

  static List<int[]> processInput(List<String[]> inputCsv)
  {

    List<int[]> inputData = convertCsvStringsToInt(inputCsv.stream());
    // identify fMin and fMax
    int numberOfDataColumns = inputCsv.get(0).length - (PRE_DATA_ROWS + POST_DATA_ROWS);
    Integer fMin[] = new Integer[numberOfDataColumns];
    Integer fMax[] = new Integer[numberOfDataColumns];
    identifyFminFmax(inputData, fMin, fMax);

    return cleanData(inputData, fMin, fMax);
  }

  /**
   * Intends output data using tabs and ads original header.
   * @param header
   * @param result
   * @return
   */
  private String formatOutput(String[] header, List<int[]> result)
  {
    StringBuilder builder = new StringBuilder();
    builder.append(Arrays.stream(header).collect(Collectors.joining(TAB_DELIMITER)));
    for (int[] row : result)
    {
      builder.append(LINE_SEPARATOR);
      builder.append(Arrays.stream(row).mapToObj(String::valueOf).collect(Collectors.joining(TAB_DELIMITER)));
    }

    return builder.toString();
  }

  /**
   * The app should parse the csv file and remove any records (rows) which meet both of the following conditions:
   * Have a Decision of 0
   * For each variable (column), no value falls between FMIN and FMAX.
   *
   * @param inputCsvData
   * @param fMin
   * @param fMax
   * @return
   */
  private static List<int[]> cleanData(List<int[]> inputCsvData, Integer[] fMin, Integer[] fMax)
  {
    List<int[]> result = new ArrayList<>();
    for (int[] element : inputCsvData)
    {
      if (element[element.length - POST_DATA_ROWS] == DECISION_0)
      {
        for (int k = 0; k < fMax.length; k++)
        {
          if (element[k + PRE_DATA_ROWS] >= fMin[k] && element[k + PRE_DATA_ROWS] <= fMax[k])
          {
            result.add(element);
            break;
          }
        }
      }
      else
      {
        result.add(element);
      }
    }
    return result;
  }

  /**
   * Where FMIN and FMAX are the smallest and largest value for that variable across all records with a decision value of 1.
   * @param result
   * @param fMin
   * @param fMax
   */
  private static void identifyFminFmax(List<int[]> result, Integer[] fMin, Integer[] fMax)
  {

    result.forEach(elements -> {
      for (int k = 0; k < fMin.length; k++)
      {
        if (elements[elements.length - POST_DATA_ROWS] != 1)
        {
          continue;
        }
        int currentElement = elements[k + PRE_DATA_ROWS];
        if (fMin[k] == null || currentElement < fMin[k])
        {
          fMin[k] = currentElement;
        }
        if (fMax[k] == null || currentElement > fMax[k])
        {
          fMax[k] = currentElement;
        }
      }

    });
  }

  /**
   * For each line in csv file produce string array for the row.
   * @param input
   * @return
   */
  static List<String[]> parseCsvDataAsString(Stream<String> input)
  {

    return input.map(line -> line.split(CSV_COMMA_SEPARATOR)).collect(Collectors.toList());
  }

  /**
   * Convert arrays of strings in the input stream to the list of integer arrays. First header value skipped.
   * @param input
   * @return
   */
  static List<int[]> convertCsvStringsToInt(Stream<String[]> input)
  {
    return input.skip(1).map(row -> Stream.of(row).mapToInt(Integer::parseInt).toArray()).collect(Collectors.toList());
  }

}
