<!DOCTYPE html>
<html lang="en">
<head>
    <title>File Upload</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<%--TODO: add fluid layout--%>
<body>
<div>
    <form method="POST" action="fileupload" enctype="multipart/form-data">
        Select CSV file to upload:
        <input type="file" name="file" id="file"/>
        <input type="submit" value="Upload" name="upload" id="upload"/>
    </form>
</div>
</body>
</html>
